#THIS APPLICATION WOULD BE FAR MORE ROBUST IF USING THE NLTK LIBRARY AVAILABLE AT http://www.nltk.org/, FOR THE PURPOSES OF DEMONSTRATING TECHNICAL ABILITY WE WILL BE HARDCODING ENGLISH STOPWORDS INTO THE APPLICATION

import os
import string
from collections import Counter
from collections import defaultdict


class Token:
    def __init__(self, value):
        self.value = value
        self.count = 1
        self.documents = []
        self.sentences = []

    def add_document(self, document):
        if document not in self.documents:
            self.documents.append(document)

    def add_sentence(self, sentence):
        if sentence not in self.sentences:
            self.sentences.append(sentence)

def output():
    print("<table>")
    print("<tr><th>Word(#)</th><th>Documents</th><th>Sentences containing the word</th></tr>")

    for w in sorted(outlist.items(), key=lambda t: t[1].count, reverse=True):
        print("<tr><td>"+w[1].value + " (#" +str(w[1].count) + ")" + "</td>")
        print("<td>"+', '.join(w[1].documents)+"</td>")
        print("<td><div style='max-height:200px;overflow-y:scroll;border:1px solid black;margin-top:10px;'><p>"+'.</p><p>'.join(w[1].sentences).replace(w[1].value, "<b>"+w[1].value+"</b>")+"</p></div></td></tr>")
    print("</table>")




translator = str.maketrans('', '', string.punctuation)

#STOPWORDS TAKEN FROM http://www.ranks.nl/stopwords
#LARGE LIST USED TO TRY TO ENCOURAGE MEANINGFUL HASHTAGS
sw = open('stopwords')
sw = sw.read().split()
outlist = defaultdict(Token)

for filename in os.listdir('test docs'):
     f = open('test docs/'+filename, 'r', encoding="utf-8")
     content = f.read().lower() #convert text to lower to ensure duplicates are counted and matching on stopwords occurs correctly

     sentences = content.split('.') #split into sentences

     for sentence in sentences:
        sentence = sentence.translate(translator) #remove all other punctionation
        words = sentence.split() #tokenize
        filtered_words = [word for word in words if word not in sw] #remove stopwords
        for fword in filtered_words:
            if fword in outlist:
                outlist[fword].count += 1
                outlist[fword].add_document(filename)
                outlist[fword].add_sentence(sentence)
            else:
                outlist[fword] = Token(fword)
                outlist[fword].add_document(filename)
                outlist[fword].add_sentence(sentence)

output()
#for w in sorted(outlist.items(), key=lambda t: t[1].count, reverse=True):
    #print("(" + str(w[1].count) + ") " + w[1].value)
